import 'package:flutter/material.dart';
import 'package:mu_app/Screens/welcomeScreen.dart';

void main() {
  runApp(MyApp());
}

class MyApp extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: "AmongUS",
      theme: ThemeData(primaryColor: const Color.fromARGB(255, 170, 26, 16)),
      home: const WelcomeScreen(),
    );
  }
}
