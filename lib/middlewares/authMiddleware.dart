import 'dart:convert';
import 'dart:io';
import 'package:flutter_secure_storage/flutter_secure_storage.dart';
import 'package:firebase_auth/firebase_auth.dart';
import 'package:http/http.dart';

final storage = new FlutterSecureStorage();

// String baseUrl = "localhost:8080";
String baseUrl = "54.179.112.129:8080";

// var client = HttpClient();

login(String email, String pass) async {
  try {
    final data = {'email': email, 'password': pass};
    final dataEncode = jsonEncode(data);
    Response response = await post(Uri.http(baseUrl, "/api/v1/users/login"),
        headers: {
          'Content-Type': 'application/json; charset=UTF-8',
        },
        body: dataEncode);
    print(response.statusCode);
    if (response.statusCode == 401) {
      return response.statusCode;
    } else if (response.statusCode == 404) {
      var data = jsonDecode(response.body.toString());
      return response.statusCode;
    } else if (response.statusCode == 200) {
      var data = jsonDecode(response.body.toString());
      await storage.write(key: 'jwt', value: data);
      print("Login Success");
      return response.statusCode;
    } else {
      print(response.body);
    }
  } catch (e) {
    print(e.toString());
  }
}

logOut() async {
  await storage.delete(key: 'jwt');
}

register(email, username, pass, conPass, firstname, lastname) async {
  final data = {
    'email': email,
    'password': pass,
    'confirmPassword': conPass,
    'username': username,
    'firstname': firstname,
    'lastname': lastname
  };
  final dataEncode = jsonEncode(data);
  Response response = await post(Uri.http(baseUrl, "/api/v1/users/register"),
      headers: {
        'Content-Type': 'application/json; charset=UTF-8',
      },
      body: dataEncode);
  if (response.statusCode == 401) {
    return response.statusCode;
  } else if (response.statusCode == 200) {
    print("Register Success");
    return response.statusCode;
  }
}

getUserInfo() async {
  var token = await storage.read(key: 'jwt');
  Response response = await get(Uri.http(baseUrl, "/api/v1/users/"), headers: {
    'Content-Type': 'application/json; charset=UTF-8',
    'Authorization': 'Bearer $token'
  });
  return response;
}
