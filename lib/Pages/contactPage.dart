import 'dart:convert';

import 'package:contactus/contactus.dart';
import 'package:flutter/material.dart';
import 'package:mu_app/middlewares/authMiddleware.dart';

final email = TextEditingController();
final pass = TextEditingController();
final conPass = TextEditingController();
final username = TextEditingController();

class UserInfo {
  final String email;
  final String username;
  final String firstname;
  final String lastname;

  const UserInfo({
    required this.email,
    required this.username,
    required this.firstname,
    required this.lastname,
  });

  factory UserInfo.fromJson(Map<String, dynamic> json) {
    return UserInfo(
      email: json['email'],
      username: json['username'],
      firstname: json['firstname'],
      lastname: json['lastname'],
    );
  }
}

Future<UserInfo> fetchUser() async {
  var token = await storage.read(key: 'jwt');
  final response = await getUserInfo();
  if (response.statusCode == 200) {
    // If the server did return a 200 OK response,
    // then parse the JSON.
    return UserInfo.fromJson(jsonDecode(response.body));
  } else {
    // If the server did not return a 200 OK response,
    // then throw an exception.
    throw Exception('Failed to load album');
  }
}

class MyContactPage extends StatefulWidget {
  const MyContactPage({super.key});

  @override
  State<MyContactPage> createState() => _MyContactPageState();
}

class _MyContactPageState extends State<MyContactPage> {
  late Future<UserInfo> futureUserInfo;

  @override
  void initState() {
    super.initState();
    futureUserInfo = fetchUser();
  }

  @override
  Widget build(
    BuildContext context,
  ) {
    return MaterialApp(
      debugShowCheckedModeBanner: false,
      home: Scaffold(
          backgroundColor: const Color.fromARGB(255, 255, 255, 255),
          body: FutureBuilder<UserInfo>(
              future: futureUserInfo,
              builder: (context, snapshot) {
                if (snapshot.hasData) {
                  return ContactUs(
                    emailText: '${snapshot.data!.email}',
                    companyName:
                        '${snapshot.data!.firstname} ${snapshot.data!.lastname}',
                    dividerThickness: 2,
                    tagLine: 'Flutter Developer',
                    cardColor: const Color.fromARGB(255, 204, 204, 204),
                    companyColor: const Color.fromARGB(255, 45, 45, 45),
                    taglineColor: const Color.fromARGB(255, 29, 29, 29),
                    textColor: const Color.fromARGB(255, 62, 62, 62),
                    email: '',
                  );
                  ;
                } else if (snapshot.hasError) {
                  return Text('${snapshot.error}');
                }

                // By default, show a loading spinner.
                return const CircularProgressIndicator();
              })),
    );
  }
}
