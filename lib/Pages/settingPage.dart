import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter/src/widgets/placeholder.dart';
import 'package:flutter/src/widgets/framework.dart';
import 'package:settings_ui/settings_ui.dart';

class MySettingPage extends StatefulWidget {
  const MySettingPage({super.key});

  @override
  State<MySettingPage> createState() => _MySettingPageState();
}

class _MySettingPageState extends State<MySettingPage> {
  @override
  Widget build(BuildContext context) {
    bool isSwitched = false;
    return SettingsList(
      sections: [
        SettingsSection(
          title: const Text('Section 1'),
          tiles: [
            SettingsTile(
              title: const Text('Language'),
              value: const Text('English'),
              leading: const Icon(Icons.language),
              onPressed: (BuildContext context) {},
            ),
            SettingsTile.switchTile(
              title: const Text('Use System Theme'),
              leading: const Icon(Icons.phone_android),
              initialValue: isSwitched,
              onToggle: (value) {
                setState(() {
                  isSwitched = value;
                });
              },
            ),
          ],
        ),
        SettingsSection(
          title: const Text('Section 2'),
          tiles: [
            SettingsTile(
              title: const Text('Security'),
              value: const Text('Fingerprint'),
              leading: const Icon(Icons.lock),
              onPressed: (BuildContext context) {},
            ),
            SettingsTile.switchTile(
              title: const Text('Use fingerprint'),
              leading: const Icon(Icons.fingerprint),
              initialValue: true,
              onToggle: (value) {},
            ),
          ],
        ),
      ],
    );
  }
}
