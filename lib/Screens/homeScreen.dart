import 'package:flutter/material.dart';
import 'package:flutter/src/widgets/framework.dart';
import 'package:flutter/src/widgets/placeholder.dart';
import 'package:mu_app/Pages/contactPage.dart';
import 'package:mu_app/Pages/profilePage.dart';
import 'package:mu_app/Pages/searchPage.dart';
import 'package:mu_app/Pages/settingPage.dart';
import 'package:settings_ui/settings_ui.dart';

class MyHomeScreen extends StatefulWidget {
  const MyHomeScreen({super.key});

  @override
  State<MyHomeScreen> createState() => _MyHomeScreenState();
}

class _MyHomeScreenState extends State<MyHomeScreen> {
  int _selectedIndex = 0;
  static const TextStyle optionStyle =
      TextStyle(fontSize: 30, fontWeight: FontWeight.bold, color: Colors.white);
  static final List<Widget> _widgetOptions = <Widget>[
    const Text(
      'Index 0: Home',
      style: optionStyle,
    ),
    const Text(
      'Index 1: About',
      style: optionStyle,
    ),
    const Profile1(),
    const MyContactPage(),
    const MySettingPage(),
  ];

  void _onItemTapped(int index) {
    setState(() {
      _selectedIndex = index;
    });
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        primary: true,
        title: Row(
          mainAxisAlignment: MainAxisAlignment.center,
          children: [
            const Text("       "),
            Image.asset(
              "./assets/images/logo.png",
              fit: BoxFit.contain,
              height: 32,
              width: 32,
            ),
            Container(
              child: const Text(
                "AmoungUS",
                style: TextStyle(fontWeight: FontWeight.bold),
              ),
            )
          ],
        ),
        actions: [
          // Navigate to the Search Screen
          IconButton(
              onPressed: () => Navigator.of(context)
                  .push(MaterialPageRoute(builder: (_) => const SearchPage())),
              icon: const Icon(Icons.search))
        ],
        backgroundColor: const Color.fromARGB(255, 193, 13, 0),
      ),
      body: Center(
        child: _widgetOptions.elementAt(_selectedIndex),
      ),
      bottomNavigationBar: Theme(
        data: Theme.of(context)
            .copyWith(canvasColor: const Color.fromARGB(255, 193, 13, 0)),
        child: BottomNavigationBar(
          items: const <BottomNavigationBarItem>[
            BottomNavigationBarItem(
              icon: Icon(Icons.home),
              label: 'Home',
            ),
            BottomNavigationBarItem(
              icon: Icon(Icons.info),
              label: 'About',
            ),
            BottomNavigationBarItem(
              icon: Icon(Icons.person),
              label: 'Profile',
            ),
            BottomNavigationBarItem(
              icon: Icon(Icons.contact_mail),
              label: 'Contact',
            ),
            BottomNavigationBarItem(
              icon: Icon(Icons.settings),
              label: 'Settings',
            ),
          ],
          currentIndex: _selectedIndex,
          selectedItemColor: const Color.fromARGB(255, 255, 172, 172),
          // fixedColor: const Color.fromARGB(255, 193, 13, 0),
          unselectedItemColor: Colors.white,
          onTap: _onItemTapped,
        ),
      ),
      backgroundColor: const Color.fromARGB(255, 20, 20, 20),
    );
  }
}
