import 'package:flutter/material.dart';
import 'package:email_validator/email_validator.dart';
import 'package:mu_app/Screens/homeScreen.dart';
import 'package:mu_app/Screens/registerScreen.dart';

import '../middlewares/authMiddleware.dart';

final email = TextEditingController();
final pass = TextEditingController();

class MyLoginForm extends StatefulWidget {
  const MyLoginForm({super.key});

  @override
  State<MyLoginForm> createState() => _MyLoginFormState();
}

class _MyLoginFormState extends State<MyLoginForm> {
  // Initially password is obscure
  bool _isObscure = true;

  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: const EdgeInsets.fromLTRB(50, 0, 50, 0),
      child: Column(
        mainAxisAlignment: MainAxisAlignment.center,
        children: [
          Image.asset(
            './assets/images/logo.png',
            scale: 15,
          ),
          // const Text(
          //   "AmoungUS",
          //   style: TextStyle(
          //       fontSize: 20, fontWeight: FontWeight.bold, color: Colors.white),
          // ),
          const SizedBox(
            height: 30,
          ),
          TextField(
            controller: email,
            keyboardType: TextInputType.emailAddress,
            autofocus: false,
            decoration: const InputDecoration(
                fillColor: Colors.white,
                icon: Icon(Icons.email_outlined),
                iconColor: Color.fromARGB(255, 193, 13, 0),
                border: OutlineInputBorder(
                  borderSide: BorderSide(
                      width: 3,
                      color: Color.fromARGB(255, 193, 13, 0)), //<-- SEE HERE
                ),
                labelText: "Email",
                labelStyle: TextStyle(color: Colors.white)),
            style: const TextStyle(
                color: Colors.white, decorationColor: Colors.white),
          ),
          const SizedBox(
            height: 20,
          ),
          TextField(
            controller: pass,
            decoration: InputDecoration(
                icon: const Icon(Icons.lock_outline),
                iconColor: const Color.fromARGB(255, 193, 13, 0),
                border: const OutlineInputBorder(
                  borderSide: BorderSide(
                      style: BorderStyle.none,
                      width: 3,
                      color: Color.fromARGB(255, 193, 13, 0)), //<-- SEE HERE
                ),
                labelText: "Password",
                suffixIcon: IconButton(
                  onPressed: () {
                    setState(() {
                      _isObscure = !_isObscure;
                    });
                  },
                  icon: Icon(
                      _isObscure ? Icons.visibility : Icons.visibility_off),
                  color: const Color.fromARGB(255, 193, 13, 0),
                ),
                labelStyle: const TextStyle(color: Colors.white)),
            style: const TextStyle(
                color: Colors.white, decorationColor: Colors.white),
            obscureText: _isObscure,
          ),
          const SizedBox(
            height: 20,
          ),
          SizedBox(
            child: ElevatedButton.icon(
                onPressed: () async {
                  if (email.text == null || email.text.isEmpty) {
                    showDialog<String>(
                        context: context,
                        builder: (BuildContext context) => AlertDialog(
                                title: const Text('Email is empty'),
                                content: const Text('Please fill your Email'),
                                actions: <Widget>[
                                  TextButton(
                                    onPressed: () =>
                                        Navigator.pop(context, 'OK'),
                                    child: const Text('OK'),
                                  )
                                ]));
                  } else if (EmailValidator.validate(email.text) == false) {
                    showDialog<String>(
                        context: context,
                        builder: (BuildContext context) => AlertDialog(
                                title: const Text('Email is Invalid'),
                                content: const Text('Please fill valid Email'),
                                actions: <Widget>[
                                  TextButton(
                                    onPressed: () =>
                                        Navigator.pop(context, 'OK'),
                                    child: const Text('OK'),
                                  )
                                ]));
                  } else if (pass.text == null || pass.text.isEmpty) {
                    showDialog<String>(
                        context: context,
                        builder: (BuildContext context) => AlertDialog(
                                title: const Text('Password is Empty'),
                                content:
                                    const Text('Please fill your Password'),
                                actions: <Widget>[
                                  TextButton(
                                    onPressed: () =>
                                        Navigator.pop(context, 'OK'),
                                    child: const Text('OK'),
                                  )
                                ]));
                  } else {
                    final resLogin = await login(email.text, pass.text);
                    print(resLogin);
                    if (resLogin == 401) {
                      // ignore: use_build_context_synchronously
                      showDialog<String>(
                          context: context,
                          builder: (BuildContext context) => AlertDialog(
                                  title: const Text('Password Incorrect'),
                                  content: const Text('Please try again'),
                                  actions: <Widget>[
                                    TextButton(
                                      onPressed: () =>
                                          Navigator.pop(context, 'OK'),
                                      child: const Text('OK'),
                                    )
                                  ]));
                    } else if (resLogin == 404) {
                      // ignore: use_build_context_synchronously
                      showDialog<String>(
                          context: context,
                          builder: (BuildContext context) => AlertDialog(
                                  title: const Text('Not Found This Email'),
                                  content: const Text('Please try again'),
                                  actions: <Widget>[
                                    TextButton(
                                      onPressed: () =>
                                          Navigator.pop(context, 'OK'),
                                      child: const Text('OK'),
                                    )
                                  ]));
                    } else if (resLogin == 200) {
                      email.clear();
                      pass.clear();
                      // ignore: use_build_context_synchronously
                      Navigator.pushReplacement(
                          context,
                          MaterialPageRoute(
                              builder: (context) => const MyHomeScreen()));
                    }
                  }
                },
                icon: const Icon(Icons.login_outlined),
                style: ElevatedButton.styleFrom(
                    backgroundColor: const Color.fromARGB(255, 193, 13, 0)),
                label: const Text(
                  "Login",
                  style: TextStyle(fontSize: 19),
                )),
          ),
          SizedBox(
              child: ListTile(
            title: Row(
              mainAxisAlignment: MainAxisAlignment.center,
              children: [
                const Text(
                  "Does not have account?",
                  style: TextStyle(color: Colors.white),
                ),
                TextButton(
                    onPressed: () {
                      email.clear();
                      pass.clear();
                      Navigator.push(
                          context,
                          MaterialPageRoute(
                              builder: (context) => const MyRegisterScreen()));
                    },
                    child: const Text(
                      "Sign Up",
                      style: TextStyle(color: Color.fromARGB(255, 193, 13, 0)),
                    )),
              ],
            ),
          ))
        ],
      ),
    );
  }
}
