import 'package:email_validator/email_validator.dart';
import 'package:flutter/material.dart';
import 'package:flutter/src/widgets/framework.dart';
import 'package:flutter/src/widgets/placeholder.dart';
import 'package:mu_app/Screens/welcomeScreen.dart';
import 'package:mu_app/middlewares/authMiddleware.dart';

final email = TextEditingController();
final pass = TextEditingController();
final conPass = TextEditingController();
final username = TextEditingController();
final firstname = TextEditingController();
final lastname = TextEditingController();

bool isChecked = false;

class MyRegisterForm extends StatefulWidget {
  const MyRegisterForm({super.key});

  @override
  State<MyRegisterForm> createState() => _MyRegisterFormState();
}

class _MyRegisterFormState extends State<MyRegisterForm> {
  bool _isObscure = true;
  bool _isObscureCon = true;

  @override
  Widget build(BuildContext context) {
    Color getColor(Set<MaterialState> states) {
      const Set<MaterialState> interactiveStates = <MaterialState>{
        MaterialState.pressed,
        MaterialState.hovered,
        MaterialState.focused,
      };
      if (states.any(interactiveStates.contains)) {
        return Colors.blue;
      }
      return const Color.fromARGB(255, 193, 13, 0);
    }

    return Padding(
      padding: const EdgeInsets.fromLTRB(50, 130, 50, 0),
      child: Column(
        children: [
          const Text(
            "Sign Up",
            style: TextStyle(
                fontSize: 50,
                color: Color.fromARGB(255, 193, 13, 0),
                fontWeight: FontWeight.bold),
          ),
          const SizedBox(
            height: 80,
          ),
          TextField(
            controller: email,
            keyboardType: TextInputType.emailAddress,
            autofocus: false,
            decoration: const InputDecoration(
                fillColor: Colors.white,
                border: OutlineInputBorder(
                  borderSide: BorderSide(
                      width: 3,
                      color: Color.fromARGB(255, 52, 3, 0)), //<-- SEE HERE
                ),
                icon: Icon(Icons.email_outlined),
                iconColor: Color.fromARGB(255, 193, 13, 0),
                labelText: "Email",
                labelStyle: TextStyle(color: Colors.white)),
            style: const TextStyle(
                color: Colors.white, decorationColor: Colors.white),
          ),
          const SizedBox(
            height: 20,
          ),
          TextField(
            controller: username,
            decoration: const InputDecoration(
                fillColor: Colors.white,
                border: OutlineInputBorder(
                  borderSide: BorderSide(
                      width: 3,
                      color: Color.fromARGB(255, 52, 3, 0)), //<-- SEE HERE
                ),
                icon: Icon(Icons.account_box_outlined),
                iconColor: Color.fromARGB(255, 193, 13, 0),
                labelText: "Username",
                labelStyle: TextStyle(color: Colors.white)),
            style: const TextStyle(
                color: Colors.white, decorationColor: Colors.white),
          ),
          const SizedBox(
            height: 20,
          ),
          TextField(
            controller: pass,
            decoration: InputDecoration(
                fillColor: Colors.white,
                border: const OutlineInputBorder(
                  borderSide: BorderSide(
                      width: 3,
                      color: Color.fromARGB(255, 52, 3, 0)), //<-- SEE HERE
                ),
                labelText: "Password",
                labelStyle: const TextStyle(color: Colors.white),
                icon: const Icon(Icons.lock_outline),
                iconColor: const Color.fromARGB(255, 193, 13, 0),
                suffixIcon: IconButton(
                  onPressed: () {
                    setState(() {
                      _isObscure = !_isObscure;
                    });
                  },
                  icon: Icon(
                      _isObscure ? Icons.visibility : Icons.visibility_off),
                  color: const Color.fromARGB(255, 193, 13, 0),
                )),
            style: const TextStyle(
                color: Colors.white, decorationColor: Colors.white),
            obscureText: _isObscure,
          ),
          const SizedBox(
            height: 20,
          ),
          TextField(
            controller: conPass,
            decoration: InputDecoration(
              fillColor: Colors.white,
              border: const OutlineInputBorder(
                borderSide: BorderSide(
                    width: 3,
                    color: Color.fromARGB(255, 52, 3, 0)), //<-- SEE HERE
              ),
              labelText: "Confirm Password",
              labelStyle: const TextStyle(color: Colors.white),
              icon: const Icon(Icons.lock_outline),
              iconColor: const Color.fromARGB(255, 193, 13, 0),
              suffixIcon: IconButton(
                onPressed: () {
                  setState(() {
                    _isObscureCon = !_isObscureCon;
                  });
                },
                icon: Icon(
                    _isObscureCon ? Icons.visibility : Icons.visibility_off),
                color: const Color.fromARGB(255, 193, 13, 0),
              ),
            ),
            style: const TextStyle(
                color: Colors.white, decorationColor: Colors.white),
            obscureText: _isObscureCon,
          ),
          const SizedBox(
            height: 20,
          ),
          Row(
            children: [
              const Icon(
                  color: Color.fromARGB(255, 193, 13, 0),
                  Icons.account_box_outlined),
              const SizedBox(
                width: 20,
              ),
              Expanded(
                child: TextField(
                  controller: firstname,
                  decoration: const InputDecoration(
                    fillColor: Colors.white,
                    border: OutlineInputBorder(
                      borderSide: BorderSide(
                          width: 3,
                          color: Color.fromARGB(255, 52, 3, 0)), //<-- SEE HERE
                    ),
                    labelText: "Firstname",
                    labelStyle: TextStyle(color: Colors.white),
                  ),
                  style: const TextStyle(
                      color: Colors.white, decorationColor: Colors.white),
                ),
              ),
              const SizedBox(
                width: 20,
              ),
              Expanded(
                  child: TextField(
                controller: lastname,
                keyboardType: TextInputType.emailAddress,
                autofocus: false,
                decoration: const InputDecoration(
                  fillColor: Colors.white,
                  border: OutlineInputBorder(
                    borderSide: BorderSide(
                        width: 3,
                        color: Color.fromARGB(255, 52, 3, 0)), //<-- SEE HERE
                  ),
                  labelText: "Lastname",
                  labelStyle: TextStyle(color: Colors.white),
                ),
                style: const TextStyle(
                    color: Colors.white, decorationColor: Colors.white),
              ))
            ],
          ),
          Row(
            mainAxisAlignment: MainAxisAlignment.center,
            children: [
              Checkbox(
                  fillColor: MaterialStateProperty.resolveWith(getColor),
                  checkColor: Colors.black,
                  value: isChecked,
                  onChanged: (bool? value) {
                    setState(() {
                      isChecked = !isChecked;
                    });
                  }),
              const Text(
                "Agree With",
                style: TextStyle(color: Colors.white),
              ),
              TextButton(
                  onPressed: () {},
                  child: const Text(
                    "Terms & Conditions",
                    style: TextStyle(color: Color.fromARGB(255, 193, 13, 0)),
                  ))
            ],
          ),
          const SizedBox(
            height: 20,
          ),
          SizedBox(
            height: 50,
            width: 150,
            child: ElevatedButton(
              style: ElevatedButton.styleFrom(
                  backgroundColor: const Color.fromARGB(255, 193, 13, 0)),
              // ignore: avoid_returning_null_for_void
              onPressed: isChecked
                  ? () async {
                      if (email.text.isEmpty) {
                        showDialog<String>(
                            context: context,
                            builder: (BuildContext context) => AlertDialog(
                                    title: const Text('Email is empty'),
                                    content:
                                        const Text('Please fill your Email'),
                                    actions: <Widget>[
                                      TextButton(
                                        onPressed: () =>
                                            Navigator.pop(context, 'OK'),
                                        child: const Text('OK'),
                                      )
                                    ]));
                      } else if (EmailValidator.validate(email.text) == false) {
                        showDialog<String>(
                            context: context,
                            builder: (BuildContext context) => AlertDialog(
                                    title: const Text('Email is Invalid'),
                                    content:
                                        const Text('Please fill valid Email'),
                                    actions: <Widget>[
                                      TextButton(
                                        onPressed: () =>
                                            Navigator.pop(context, 'OK'),
                                        child: const Text('OK'),
                                      )
                                    ]));
                      } else if (username.text.isEmpty) {
                        showDialog<String>(
                            context: context,
                            builder: (BuildContext context) => AlertDialog(
                                    title: const Text('Username is empty'),
                                    content:
                                        const Text('Please fill your Username'),
                                    actions: <Widget>[
                                      TextButton(
                                        onPressed: () =>
                                            Navigator.pop(context, 'OK'),
                                        child: const Text('OK'),
                                      )
                                    ]));
                      } else if (pass.text.isEmpty) {
                        showDialog<String>(
                            context: context,
                            builder: (BuildContext context) => AlertDialog(
                                    title: const Text('Password is empty'),
                                    content:
                                        const Text('Please fill your Password'),
                                    actions: <Widget>[
                                      TextButton(
                                        onPressed: () =>
                                            Navigator.pop(context, 'OK'),
                                        child: const Text('OK'),
                                      )
                                    ]));
                      } else if (conPass.text.isEmpty) {
                        showDialog<String>(
                            context: context,
                            builder: (BuildContext context) => AlertDialog(
                                    title:
                                        const Text('Confirm Password is empty'),
                                    content: const Text(
                                        'Please fill your Confirm Password'),
                                    actions: <Widget>[
                                      TextButton(
                                        onPressed: () =>
                                            Navigator.pop(context, 'OK'),
                                        child: const Text('OK'),
                                      )
                                    ]));
                      } else if (firstname.text.isEmpty ||
                          lastname.text.isEmpty) {
                        showDialog<String>(
                            context: context,
                            builder: (BuildContext context) => AlertDialog(
                                    title: const Text(
                                        'Firstname or Lastname is empty'),
                                    content: const Text(
                                        'Please fill your Firstname or Lastname'),
                                    actions: <Widget>[
                                      TextButton(
                                        onPressed: () =>
                                            Navigator.pop(context, 'OK'),
                                        child: const Text('OK'),
                                      )
                                    ]));
                      } else if (pass.text != conPass.text) {
                        showDialog<String>(
                            context: context,
                            builder: (BuildContext context) => AlertDialog(
                                    title: const Text(
                                        'Password Not match with Confirm Password'),
                                    content: const Text('Please try again.'),
                                    actions: <Widget>[
                                      TextButton(
                                        onPressed: () =>
                                            Navigator.pop(context, 'OK'),
                                        child: const Text('OK'),
                                      )
                                    ]));
                      } else {
                        final resRegister = await register(
                            email.text,
                            username.text,
                            pass.text,
                            conPass.text,
                            firstname.text,
                            lastname.text);
                        if (resRegister == 200) {
                          isChecked = false;
                          email.clear();
                          pass.clear();
                          conPass.clear();
                          username.clear();
                          firstname.clear();
                          lastname.clear();
                          // ignore: use_build_context_synchronously
                          Navigator.pushReplacement(
                              context,
                              MaterialPageRoute(
                                  builder: (context) => const WelcomeScreen()));
                        } else if (resRegister == 401) {
                          // ignore: use_build_context_synchronously
                          showDialog<String>(
                              context: context,
                              builder: (BuildContext context) => AlertDialog(
                                      title: const Text('Email Already Exit'),
                                      content: const Text(
                                          'Please use anothor email and try again'),
                                      actions: <Widget>[
                                        TextButton(
                                          onPressed: () =>
                                              Navigator.pop(context, 'OK'),
                                          child: const Text('OK'),
                                        )
                                      ]));
                        }
                      }
                    }
                  : null,
              child: const Text(
                "Sign Up",
                style: TextStyle(fontSize: 25, fontWeight: FontWeight.bold),
              ),
            ),
          )
        ],
      ),
    );
  }
}
